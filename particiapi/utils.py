#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

import secrets
from functools import wraps

from flask import request, session, make_response, current_app

from .problemdetails import ProblemDetails, problem_details_response


def config_bool(s):
    if isinstance(s, str):
        return s.lower() in ("true", "yes")
    return bool(s)


def content_type(content_types):
    def decorator_content_type(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if (
                request.method not in ("HEAD", "GET", "OPTIONS")
                and request.content_type not in content_types
            ):
                return problem_details_response(
                    415,
                    ProblemDetails.UNSUPPORTED_CONTENT_TYPE,
                    f"expected content types: {','.join(content_types)}"
                )

            return f(*args, **kwargs)

        return wrapper

    return decorator_content_type


def cache_control(**cache_kwargs):
    cache_control_attrs = set([
        "no_cache",
        "no_store",
        "max_age",
        "no_transform",
        "immutable",
        "must_revalidate",
        "private",
        "proxy_revalidate",
        "public",
        "s_maxage",
    ])
    for k in cache_kwargs.keys():
        if k not in cache_control_attrs:
            raise ValueError(f"invalid argument: {k}")

    def decorator_cache_control(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            response = make_response(f(*args, **kwargs))
            if request.method == "GET" and 200 <= response.status_code <= 299:
                for k, v in cache_kwargs.items():
                    setattr(response.cache_control, k, v)
            return response

        return wrapper

    return decorator_cache_control


def auth_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if (
            not config_bool(
                current_app.config.get("AUTHENTICATION_DISABLED")
            ) and
            not session.get("authenticated")
        ):
            return problem_details_response(
                403,
                ProblemDetails.AUTHENTICATION_REQUIRED
            )

        return f(*args, **kwargs)

    return wrapper


def session_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if session.get("uid") is None:
            return problem_details_response(
                403,
                ProblemDetails.SESSION_REQUIRED
            )

        return f(*args, **kwargs)

    return auth_required(wrapper)


def generate_csrf_token():
    return secrets.token_urlsafe(16)


def csrf_check(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        session_csrf_token = session.get("csrf_token")
        header_csrf_token = request.headers.get("X-CSRF-Token")

        if (
            request.method not in ("HEAD", "GET", "OPTIONS") and
            (
                not session_csrf_token or
                not header_csrf_token or
                not secrets.compare_digest(
                    session_csrf_token, header_csrf_token
                )
            )
        ):
            return problem_details_response(
                403,
                ProblemDetails.INVALID_CSRF_TOKEN,
                "the CSRF token is invalid or missing"
            )

        return f(*args, **kwargs)

    return wrapper


def have_session():
    return session.get("uid") is not None
