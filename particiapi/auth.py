#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

from flask import (
    Blueprint,
    current_app,
    make_response,
    redirect,
    render_template,
    session,
    url_for,
)
from requests.exceptions import RequestException
from authlib.integrations.base_client.errors import OAuthError

from . import oauth, models
from .utils import cache_control, generate_csrf_token


bp = Blueprint("auth", __name__, url_prefix="/auth")


@bp.route("/login", methods=["GET"])
@cache_control(max_age=3600, must_revalidate=True)
def login():
    idp = oauth.create_client("idp")

    if "uid" not in session:
        redirect_url = url_for(".callback", _external=True)
        return idp.authorize_redirect(redirect_url)

    return make_response(
        render_template(
            "auth/login.html",
            title="Login succeeded",
            message="",
            origin=current_app.config["CORS_ORIGINS"]
        )
    )


@bp.route("/callback", methods=["GET"])
def callback():
    idp = oauth.create_client("idp")
    try:
        # fetches access token including user info and validates claims
        token = idp.authorize_access_token()
        user = token.get("userinfo")
    except OAuthError as e:
        return make_response(
            render_template(
                "auth/login.html",
                title="Login failed",
                message=str(e),
                origin=current_app.config["CORS_ORIGINS"]
            ),
            403
        )

    if not user:
        try:
            user = idp.userinfo()
        except RequestException:
            return make_response(
                render_template(
                    "auth/login.html",
                    title="Login failed",
                    message="Failed to fetch userinfo",
                    origin=current_app.config["IDP_API_METADATA_URL"]
                ),
                403
            )

    session.clear()
    session.permanent = True
    session["uid"] = models.get_or_create_uid(user["iss"], user["sub"])
    session["email"] = user["email"] if user["email_verified"] else None
    session["csrf_token"] = generate_csrf_token()
    session["authenticated"] = True

    redirect_url = url_for(".login", _external=True)
    return redirect(redirect_url)
