#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

from datetime import timedelta


class ConfigDefaults:
    MAX_CONTENT_LENGTH = 4096
    SESSION_COOKIE_SECURE = True
    SESSION_COOKIE_HTTPONLY = True
    SESSION_COOKIE_SAMESITE = "None"
    PERMANENT_SESSION_LIFETIME = timedelta(days=7)
    REVERSE_PROXY_SUPPORT = False
    AUTHENTICATION_DISABLED = False
