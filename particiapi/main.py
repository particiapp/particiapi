#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

from flask import (
    Blueprint,
    render_template,
)

from .utils import cache_control


bp = Blueprint("main", __name__)


# may be used to check whether the server is running
@bp.route("/", methods=["GET"])
@cache_control(no_store=True, max_age=0)
def index():
    return render_template("main/index.html")
