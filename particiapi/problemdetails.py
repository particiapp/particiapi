#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

from enum import Enum
from dataclasses import dataclass, field, asdict

from flask import jsonify


@dataclass
class ProblemDetailsAbstractMixin:
    title: str
    type: str = field(init=False)


class ProblemDetailsMixin(ProblemDetailsAbstractMixin):
    title: str

    @property
    def type(self):
        name = getattr(self, "name", "")
        return f"tag:partici.app,2024:api:errors:{name.lower()}"


class ProblemDetails(ProblemDetailsMixin, Enum):
    MALFORMED_REQUEST = "malformed request"
    AUTHENTICATION_REQUIRED = "authentication required"
    INVALID_CSRF_TOKEN = "invalid CSRF token"
    MISSING_CSRF_TOKEN = "no CSRF token in session"
    UNSUPPORTED_CONTENT_TYPE = "unsupported content type"
    NOT_FOUND = "not found"
    EMAIL_ADDRESS_MISSING = "email address is missing"
    VOTING_NOT_ALLOWED = "voting not allowed"
    STATEMENT_EXISTS = "statement already exists"
    CONVERSATION_INACTIVE = "conversation inactive"
    STATEMENTS_NOT_ALLOWED = "statements not allowed"
    NOTIFICATIONS_NOT_AVAILABLE = "notifications not available"
    RESULTS_NOT_AVAILABLE = "results not available"
    SESSION_REQUIRED = "session required"


def problem_details_response(status, problem, detail=""):
    resp = jsonify({**asdict(problem), "status": status, "detail": detail})
    resp.content_type = "application/problem+json; charset=utf-8"
    return resp, status
