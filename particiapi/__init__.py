#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

from flask import Flask, current_app
from flask_cors import CORS
from authlib.integrations.flask_client import OAuth

from .utils import config_bool


oauth = OAuth(current_app)


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(f"{__name__}.config_defaults.ConfigDefaults")
    if test_config is None:
        # try to load config file
        app.config.from_pyfile("config.py", silent=True)
        # allow overriding settings via environment variables prefixed with
        # "PARTICIAPI_"
        app.config.from_prefixed_env(prefix="PARTICIAPI")
    else:
        # load the test config if passed in
        app.config.update(test_config)

    if app.config.get("REVERSE_PROXY_SUPPORT", False):
        from werkzeug.middleware.proxy_fix import ProxyFix
        # trusts X-Forwarded-For, X-Forwarded-Proto, X-Forwarded-Host headers
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1)

    import logging
    if app.debug:
        app.logger.setLevel(logging.DEBUG)

    from . import db
    db.init_app(app)

    if not config_bool(app.config.get("AUTHENTICATION_DISABLED")):
        oauth.init_app(app)
        metadata_url = app.config.get(
            "IDP_API_METADATA_URL",
            f"{app.config['IDP_API_BASE_URL']}/.well-known/openid-configuration"
        )
        oauth.register(
            "idp",
            server_metadata_url=metadata_url,
            client_kwargs={"scope": "openid email"},
        )

        from . import auth

        app.register_blueprint(auth.bp)

    from . import main
    from . import api

    CORS(api.bp, origins=app.config["CORS_ORIGINS"], supports_credentials=True)

    app.register_blueprint(main.bp)
    app.register_blueprint(api.bp)

    return app
