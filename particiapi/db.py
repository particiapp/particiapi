#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

import click
import psycopg
from flask import (
    current_app,
    g
)


def get_connection():
    if "db_connection" not in g:
        g.db_connection = psycopg.connect(
            host=current_app.config["DATABASE_HOST"],
            port=current_app.config["DATABASE_PORT"],
            dbname=current_app.config["DATABASE_NAME"],
            user=current_app.config["DATABASE_USER"],
            password=current_app.config["DATABASE_PASSWORD"],
            autocommit=True,
            row_factory=psycopg.rows.namedtuple_row,
        )
    return g.db_connection


def close_db(e=None):
    conn = g.pop("db_connection", None)

    if conn is not None:
        conn.close()


def init_db():
    with current_app.open_resource("schema.sql") as f:
        with get_connection().cursor() as curs:
            curs.execute(f.read().decode("utf8"))


@click.command("init-db")
def init_db_command():
    init_db()
    click.echo("database has been initilized")


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
