DROP FUNCTION IF EXISTS create_user;
CREATE FUNCTION create_user()
RETURNS TRIGGER AS $$
BEGIN
    WITH t AS (
        INSERT INTO public.users VALUES(DEFAULT) RETURNING uid
    )
    UPDATE public.particiapi_users
    SET uid = t.uid
    FROM t
    WHERE subject = NEW.subject AND issid = NEW.issid;
    RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;

CREATE TABLE IF NOT EXISTS particiapi_issuers(
    issid SERIAL PRIMARY KEY,
    issuer VARCHAR(999) NOT NULL UNIQUE,
    modified BIGINT NOT NULL DEFAULT now_as_millis(),
    created BIGINT NOT NULL DEFAULT now_as_millis()
);

CREATE TABLE IF NOT EXISTS particiapi_users(
    subject VARCHAR(999) NOT NULL,
    issid INTEGER NOT NULL REFERENCES particiapi_issuers(issid),
    uid INTEGER REFERENCES users(uid),
    modified BIGINT NOT NULL DEFAULT now_as_millis(),
    created BIGINT NOT NULL DEFAULT now_as_millis(),
    UNIQUE(subject, issid)
);

DROP TRIGGER IF EXISTS insert_new_uid
ON particiapi_users;
CREATE TRIGGER insert_new_uid
AFTER INSERT
ON particiapi_users
FOR EACH ROW
EXECUTE PROCEDURE create_user();
