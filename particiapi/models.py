#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

import datetime
import urllib
import random
from enum import IntEnum
from typing import Optional

import psycopg.errors
from psycopg import sql
from psycopg.rows import class_row
from pydantic import Field, HttpUrl
from pydantic.dataclasses import dataclass as pydantic_dataclass

from .db import get_connection as get_db_connection
from .markdown import render_markdown


MIN_VOTES_COUNT = 3


class ConversationNotFoundError(Exception):
    def __init__(self, message="conversation not found"):
        super().__init__(message)


class ConversationInactiveError(Exception):
    def __init__(self, message="conversation is inactive"):
        super().__init__(message)


class StatementNotFoundError(Exception):
    def __init__(self, message="statement not found"):
        super().__init__(message)


class NotificationsNotAvailableError(Exception):
    def __init__(
        self, message="notifications are not available for this conversation"
    ):
        super().__init__(message)


class EmailAddressMissingError(Exception):
    def __init__(self, message="user does not have an email address"):
        super().__init__(message)


class VotingNotAllowedError(Exception):
    def __init__(self, message="user is not allowed to vote on own statement"):
        super().__init__(message)


class StatementsNotAllowedError(Exception):
    def __init__(
        self, message="submitting statements is not allowed for this conversation"
    ):
        super().__init__(message)


class StatementExistsError(Exception):
    def __init__(
        self, message="a statement with identical statement already exists"
    ):
        super().__init__(message)


class ResultsNotAvailableError(Exception):
    def __init__(
        self, message="results are not available for this conversation"
    ):
        super().__init__(message)


class VoteValue(IntEnum):
    AGREE = -1
    NEUTRAL = 0
    DISAGREE = 1


@pydantic_dataclass
class Statement:
    text: str = Field(max_length=1000)
    id: Optional[int] = None
    is_meta: bool = False
    is_seed: bool = False
    last_modified: Optional[
        datetime.datetime
    ] = Field(default=None, exclude=True)


@pydantic_dataclass
class Result:
    statement_id: int = None
    statement_text: str = ""
    value: float = 0.0


@pydantic_dataclass
class GroupResults:
    agree: list[Result] = Field(default_factory=list)
    disagree: list[Result] = Field(default_factory=list)


@pydantic_dataclass
class Results:
    majority: GroupResults = Field(default_factory=GroupResults)
    groups: list[GroupResults] = Field(default_factory=list)


@pydantic_dataclass
class Conversation:
    topic: str
    description: str
    is_active: bool
    statements_allowed: bool
    notifications_available: bool
    results_available: bool
    description_html: str = ""
    last_modified: datetime.datetime = Field(exclude=True)
    seed_statements: dict[int, Statement] = Field(default_factory=dict)


@pydantic_dataclass
class Vote:
    value: VoteValue


@pydantic_dataclass
class Notifications:
    enabled: bool = False
    email: Optional[str] = None


@pydantic_dataclass
class Participant:
    statements: list[int] = Field(default_factory=list)
    votes: list[int] = Field(default_factory=list)
    notifications: Notifications = Field(default_factory=Notifications)


def single_value_row(curs):
    return lambda values: values[0]


def create_uid():
    with get_db_connection().cursor(row_factory=single_value_row) as curs:
        curs.execute(
            """INSERT INTO public.users VALUES(DEFAULT) RETURNING uid"""
        )
        return curs.fetchone()


def get_or_create_uid(issuer, subject):
    with get_db_connection().cursor(row_factory=single_value_row) as curs:
        # create issuer if needed, get issuer ID
        curs.execute(
            """
            WITH t AS (
                INSERT INTO public.particiapi_issuers (issuer)
                VALUES (%(issuer)s)
                ON CONFLICT(issuer) DO NOTHING
                RETURNING issid
            ) SELECT COALESCE (
                (
                    SELECT issid
                    FROM t
                ),
                (
                    SELECT issid
                    FROM public.particiapi_issuers
                    WHERE issuer = %(issuer)s
                )
            ) AS issid;
            """,
            {"issuer": issuer}
        )
        issid = curs.fetchone()

        # create user if needed, a polis user is created by a trigger
        curs.execute(
            """
            INSERT INTO public.particiapi_users (subject, issid)
            VALUES (%(subject)s, %(issid)s)
            ON CONFLICT(subject, issid) DO NOTHING;
            """,
            {"subject": subject, "issid": issid}
        )
        # get back the polis user created by above trigger
        curs.execute(
            """
            SELECT uid
            FROM public.particiapi_users
            WHERE subject = %(subject)s AND issid = %(issid)s;
            """,
            {"subject": subject, "issid": issid}
        )
        return curs.fetchone()


class ConversationModel:
    def __init__(self, conversation_id):
        self.conversation_id = conversation_id
        self.__zid = None

    @property
    def zid(self):
        if self.__zid is not None:
            return self.__zid

        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT zid
                FROM public.zinvites
                WHERE zinvite = %(conversation_id)s;
                """,
                {"conversation_id": self.conversation_id}
            )
            zid = curs.fetchone()

        if not zid:
            raise ConversationNotFoundError()
        self.__zid = zid
        return self.__zid

    def pid(self, uid):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT pid
                FROM public.participants
                WHERE zid = %(zid)s AND uid = %(uid)s;
                """,
                {"zid": self.zid, "uid": uid}
            )
            return curs.fetchone()

    def _ensure_pid(self, uid):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            # create extended participant entry if needed
            curs.execute(
                """
                INSERT INTO participants_extended (zid, uid)
                VALUES (%(zid)s, %(uid)s)
                ON CONFLICT(zid, uid) DO NOTHING;
                """,
                {"zid": self.zid, "uid": uid}
            )

            # create participant if needed, get participant ID
            curs.execute(
                """
                WITH t AS (
                    INSERT INTO public.participants (uid, zid)
                    VALUES (%(uid)s, %(zid)s)
                    ON CONFLICT(uid, zid) DO NOTHING
                    RETURNING pid
                ) SELECT COALESCE (
                    (
                        SELECT pid
                        FROM t
                    ),
                    (
                        SELECT pid
                        FROM public.participants
                        WHERE uid = %(uid)s AND zid = %(zid)s
                    )
                );
                """,
                {"zid": self.zid, "uid": uid}
            )
            return curs.fetchone()

    def _is_active(self):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT is_active
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            return curs.fetchone()

    def _statements_allowed(self):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT CAST(write_type AS BOOLEAN)
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            return curs.fetchone()

    def _notifications_available(self):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT CAST(subscribe_type AS BOOLEAN)
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            return curs.fetchone()

    def _strict_moderation(self):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT strict_moderation
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            return curs.fetchone()

    def _results_available(self):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT vis_type <> 0
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            return curs.fetchone()

    def get_allowed_origin(self):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT parent_url
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            parent_url = curs.fetchone()
            if parent_url is None:
                raise ConversationNotFoundError()

        try:
            u = urllib.parse.urlparse(parent_url)
        except ValueError:
            return None
        if u.scheme != "https" or not u.netloc:
            return None
        return f"{u.scheme}://{u.netloc}"

    def get_results(self):
        if not self._results_available():
            raise ResultsNotAvailableError()

        results = Results()
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """SELECT data FROM public.math_main WHERE zid = %(zid)s""",
                {"zid": self.zid}
            )
            math_data = curs.fetchone()

        statement_ids = set()

        # the polis math server may not have generated results yet
        if math_data is None:
            return results

        for type_ in ("agree", "disagree"):
            for result_data in math_data.get("consensus", {}).get(type_, []):
                if result_data["n-trials"] < MIN_VOTES_COUNT:
                    continue
                result = Result(
                    statement_id=result_data["tid"],
                    value=result_data["n-success"]/result_data["n-trials"]
                )
                getattr(results.majority, type_).append(result)
                statement_ids.add(result_data["tid"])

        for i in sorted(math_data.get("repness", [])):
            group_results = GroupResults()
            for result_data in math_data["repness"][i]:
                type_ = result_data["repful-for"]
                assert type_ in ("agree", "disagree")
                if result_data["n-trials"] < MIN_VOTES_COUNT:
                    continue
                result = Result(
                    statement_id=result_data["tid"],
                    value=result_data["n-success"]/result_data["n-trials"]
                )
                getattr(group_results, type_).append(result)
                statement_ids.add(result_data["tid"])
            results.groups.append(group_results)

        with get_db_connection().cursor() as curs:
            curs.execute(
                """
                    SELECT tid, txt
                    FROM public.comments
                    WHERE zid = %(zid)s AND tid = ANY(%(statement_ids)s)
                """,
                {
                    "zid": self.zid,
                    "statement_ids": list(statement_ids),
                }
            )
            statements = {statement_id: statement_text for statement_id, statement_text in curs}
        for group_result in [results.majority, *results.groups]:
            for type_ in ("agree", "disagree"):
                for result in getattr(group_result, type_):
                    result.statement_text = statements.get(result.statement_id, "")

        return results

    def __get_statements(self, only_pid=None, only_seeds=False):
        if only_pid is not None:
            only_pid_cond = sql.Composed([
                sql.Identifier("pid"),
                sql.SQL(" = "),
                sql.Placeholder("pid")
            ])
        else:
            only_pid_cond = sql.SQL("TRUE")
        if only_seeds:
            only_seeds_cond = sql.Composed([
                sql.Identifier("is_seed"), sql.SQL(" = TRUE")
            ])
        else:
            only_seeds_cond = sql.SQL("TRUE")
        if self._strict_moderation():
            only_approved_cond = sql.Composed([
                sql.Identifier("mod"), sql.SQL(" = 1")
            ])
        else:
            only_approved_cond = sql.Composed([
                sql.Identifier("mod"), sql.SQL(" >= 0")
            ])
        query = sql.SQL(
            """
            SELECT
                tid as id,
                txt as text,
                is_meta,
                is_seed,
                modified as last_modified
            FROM public.comments
            WHERE
                zid = %(zid)s AND
                active = TRUE AND
                {only_pid_cond} AND
                {only_seeds_cond} AND
                {only_approved_cond}
            """
        ).format(
            only_pid_cond=only_pid_cond,
            only_seeds_cond=only_seeds_cond,
            only_approved_cond=only_approved_cond,
        )

        with (
            get_db_connection().cursor(row_factory=class_row(Statement))
        ) as curs:
            statements = {
                s.id: s
                for s in curs.execute(query, {
                    "zid": self.zid, "pid": only_pid
                })
            }
        return statements

    def get_statements(self):
        return self.__get_statements()

    def get_conversation(self):
        with (
            get_db_connection().cursor(row_factory=class_row(Conversation))
        ) as curs:
            curs.execute(
                """
                SELECT topic, description, is_active,
                       CAST(write_type AS BOOLEAN) AS statements_allowed,
                       CAST(subscribe_type AS BOOLEAN) AS notifications_available,
                       vis_type <> 0 AS results_available,
                       modified as last_modified
                FROM public.conversations
                WHERE zid = %(zid)s;
                """,
                {"zid": self.zid}
            )
            conversation = curs.fetchone()
            if conversation is None:
                raise ConversationNotFoundError()

        seed_statements = self.__get_statements(only_seeds=True)
        conversation.description_html = render_markdown(conversation.description)
        conversation.seed_statements.update(seed_statements)
        return conversation

    def get_notifications(self, uid):
        with (
            get_db_connection().cursor(row_factory=class_row(Notifications))
        ) as curs:
            curs.execute(
                """
                SELECT CASE
                    WHEN pe.subscribe_email <> '' AND p.subscribed <> 0
                    THEN TRUE
                    ELSE FALSE
                END enabled, pe.subscribe_email email
                FROM public.participants_extended pe
                LEFT JOIN public.participants p
                ON pe.uid = p.uid AND p.zid = %(zid)s
                WHERE pe.uid = %(uid)s AND pe.zid = %(zid)s;
                """,
                {"uid": uid, "zid": self.zid}
            )
            notifications = curs.fetchone()
            assert notifications is not None
            return notifications

    def set_notifications(self, uid, notifications):
        if not self._is_active():
            raise ConversationInactiveError()
        if not self._notifications_available():
            raise NotificationsNotAvailableError()
        if notifications.email is None:
            raise EmailAddressMissingError()

        pid = self._ensure_pid(uid)

        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            # update email address for user (this address is used as the actual
            # recipient address)
            curs.execute(
                """
                UPDATE public.users
                SET email = %(email)s
                WHERE uid = %(uid)s
                RETURNING email;
                """,
                {
                    "uid": uid,
                    "email": notifications.email,
                }
            )
            assert curs.fetchone() == notifications.email

            # update email address for participant (this address is used for
            # the unsubscribe link in the email body)
            curs.execute(
                """
                UPDATE public.participants_extended
                SET subscribe_email = %(email)s
                WHERE zid = %(zid)s AND uid = %(uid)s
                RETURNING subscribe_email;
                """,
                {
                    "zid": self.zid,
                    "uid": uid,
                    "email": notifications.email,
                }
            )
            assert curs.fetchone() == notifications.email

            # subscribe
            curs.execute(
                """
                UPDATE public.participants
                SET subscribed = CAST(%(subscribed)s AS int)
                WHERE pid = %(pid)s AND zid = %(zid)s
                RETURNING subscribed <> 0 AS enabled;
                """,
                {
                    "pid": pid,
                    "zid": self.zid,
                    "subscribed": notifications.enabled,
                }
            )
            assert curs.fetchone() == notifications.enabled

        return self.get_notifications(uid)

    def get_participant(self, uid):
        pid = self.pid(uid)
        participant = Participant()
        if pid is None:
            return participant

        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            curs.execute(
                """
                SELECT tid
                FROM public.votes_latest_unique
                WHERE zid = %(zid)s AND pid = %(pid)s;
                """,
                {"zid": self.zid, "pid": pid}
            )
            votes = curs.fetchall()

        statements = self.__get_statements(only_pid=pid)
        if votes:
            participant.votes.extend(votes)
        participant.statements = [
            statement.id for statement in statements.values()
        ]
        participant.notifications = self.get_notifications(uid)
        return participant

    def __do_vote(self, uid, pid, tid, vote):
        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            # add new vote
            curs.execute(
                """
                INSERT INTO public.votes (zid, pid, tid, vote)
                VALUES(%(zid)s, %(pid)s, %(tid)s, %(vote)s)
                """,
                {
                    "zid": self.zid,
                    "pid": pid,
                    "tid": tid,
                    "vote": vote.value
                }
            )

            # update timestamp of participant
            curs.execute(
                """
                UPDATE public.participants
                SET last_interaction = now_as_millis(), nsli = 0
                WHERE zid = %(zid)s AND uid = %(uid)s
                RETURNING last_interaction;
                """,
                {
                    "zid": self.zid,
                    "uid": uid,
                }
            )
            modified = curs.fetchone()

            # update timestamp of conversation
            curs.execute(
                """
                UPDATE public.conversations
                SET modified = %(modified)s
                WHERE zid = %(zid)s and modified < %(modified)s;
                """,
                {
                    "modified": modified,
                    "zid": self.zid,
                }
            )

            # update number of votes of participant
            curs.execute(
                """
                UPDATE public.participants
                SET vote_count = (
                    SELECT COUNT(*)
                    FROM public.votes
                    WHERE zid = %(zid)s AND pid = %(pid)s
                )
                WHERE zid = %(zid)s AND pid = %(pid)s;
                """,
                {
                    "zid": self.zid,
                    "pid": pid,
                }
            )

        return vote

    def add_vote(self, uid, tid, vote):
        if not self._is_active():
            raise ConversationInactiveError()

        pid = self._ensure_pid(uid)

        with get_db_connection().cursor(row_factory=single_value_row) as curs:
            # check whether statement exists and was not submitted by the same
            # user
            curs.execute(
                """
                SELECT uid
                FROM public.comments
                WHERE tid = %(tid)s AND zid = %(zid)s
                """,
                {"tid": tid, "zid": self.zid}
            )
            author_uid = curs.fetchone()
            if author_uid is None:
                raise StatementNotFoundError()
            if author_uid == uid:
                raise VotingNotAllowedError()

        return self.__do_vote(uid, pid, tid, vote)

    def add_statement(self, uid, statement):
        if not self._is_active():
            raise ConversationInactiveError()
        if not self._statements_allowed():
            raise StatementsNotAllowedError()

        pid = self._ensure_pid(uid)

        with (
            get_db_connection().cursor(row_factory=class_row(Statement))
        ) as curs:
            # add statement
            try:
                curs.execute(
                    """
                    INSERT INTO public.comments (zid, pid, uid, txt)
                    VALUES(%(zid)s, %(pid)s, %(uid)s, %(txt)s)
                    RETURNING
                        tid as id,
                        txt as text,
                        is_meta,
                        is_seed,
                        modified as last_modified;
                    """,
                    {
                        "zid": self.zid,
                        "pid": pid,
                        "uid": uid,
                        "txt": statement.text
                    }
                )
            except psycopg.errors.UniqueViolation:
                # a statement with the same text already exists
                raise StatementExistsError()
            statement = curs.fetchone()

            # queue notification task for the polis server
            curs.execute(
                """
                INSERT INTO public.notification_tasks (zid)
                VALUES (%(zid)s)
                ON CONFLICT (zid)
                DO UPDATE SET modified = now_as_millis();
                """,
                {"zid": self.zid}
            )

            # vote with agree on own statement
            self.__do_vote(uid, pid, statement.id, Vote(VoteValue.AGREE))

        return statement
