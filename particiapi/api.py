#
# Copyright (C) 2024 Guido Berhoerster <guido+particiapi@berhoerster.name>
#
# Licensed under the EUPL-1.2 or later
#

import datetime
import pydantic
from flask import (
    Blueprint,
    current_app,
    jsonify,
    request,
    session,
)

from . import models
from .utils import (
    auth_required,
    cache_control,
    config_bool,
    content_type,
    csrf_check,
    generate_csrf_token,
    have_session,
    session_required,
)
from .problemdetails import (
    ProblemDetails,
    problem_details_response
)


bp = Blueprint("api", __name__, url_prefix="/api")


def dump_dataclass(type_, instance):
    return pydantic.RootModel[type_](instance).model_dump()


@bp.route("/session", methods=["POST"])
def session_():
    if not have_session():
        if not config_bool(current_app.config.get("AUTHENTICATION_DISABLED")):
            return problem_details_response(
                403,
                ProblemDetails.AUTHENTICATION_REQUIRED
            )

        if request.args.get("create", False, type=bool):
            session.clear()
            session.permanent = True
            session["uid"] = models.create_uid()
            session["email"] = None
            session["csrf_token"] = generate_csrf_token()
            session["authenticated"] = False

    return jsonify(
        csrf_token=session.get("csrf_token", ""),
        authenticated=session.get("authenticated", False),
    )


@bp.route("/conversations/<conversation_id>", methods=["GET"])
@cache_control(no_cache=True)
def conversations(conversation_id):
    try:
        cm = models.ConversationModel(conversation_id)
        c = cm.get_conversation()
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )

    resp = jsonify(dump_dataclass(models.Conversation, c))
    resp.add_etag()
    resp.last_modified = c.last_modified
    resp.make_conditional(request)
    return resp


@bp.route("/conversations/<conversation_id>/results/", methods=["GET"])
@cache_control(no_cache=True)
def results(conversation_id):
    try:
        cm = models.ConversationModel(conversation_id)
        r = cm.get_results()
    except models.ResultsNotAvailableError as exc:
        return problem_details_response(
            403, ProblemDetails.RESULTS_NOT_AVAILABLE, str(exc)
        )
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )
    resp = jsonify(dump_dataclass(models.Results, r))
    resp.add_etag()
    resp.make_conditional(request)

    return resp


@bp.route(
    "/conversations/<conversation_id>/statements/", methods=["POST"]
)
@session_required
@content_type(["application/json"])
@csrf_check
def post_statements(conversation_id):
    try:
        s = models.Statement(**request.json)
        cm = models.ConversationModel(conversation_id)
        c = cm.add_statement(session["uid"], s)
    except pydantic.ValidationError as exc:
        return problem_details_response(
            400, ProblemDetails.MALFORMED_REQUEST, str(exc)
        )
    except models.ConversationInactiveError as exc:
        return problem_details_response(
            403, ProblemDetails.CONVERSATION_INACTIVE, str(exc)
        )
    except models.StatementsNotAllowedError as exc:
        return problem_details_response(
            403, ProblemDetails.STATEMENTS_NOT_ALLOWED, str(exc)
        )
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )
    except models.StatementExistsError as exc:
        return problem_details_response(
            409, ProblemDetails.STATEMENT_EXISTS, str(exc)
        )
    return jsonify(c), 201


@bp.route(
    "/conversations/<conversation_id>/statements/", methods=["GET"]
)
@auth_required
@cache_control(no_cache=True)
def get_statements(conversation_id):
    try:
        cm = models.ConversationModel(conversation_id)
        s = cm.get_statements()
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )

    resp = jsonify(dump_dataclass(dict[int, models.Statement], s))
    resp.add_etag()
    try:
        latest_statement = max(s.values(), key=lambda key: key.last_modified)
    except ValueError:
        # no statements
        resp.last_modified = datetime.datetime.fromtimestamp(0)
    else:
        resp.last_modified = latest_statement.last_modified
    resp.make_conditional(request)

    return resp


@bp.route("/conversations/<conversation_id>/participant", methods=["GET"])
@auth_required
@cache_control(private=True, no_cache=True)
def participant(conversation_id):
    if not have_session():
        # dummy value if authentication is disabled and no session has been
        # created yet
        return jsonify(models.Participant())

    try:
        cm = models.ConversationModel(conversation_id)
        p = cm.get_participant(session["uid"])
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )
    return jsonify(p)


@bp.route(
    "/conversations/<conversation_id>/participant/notifications",
    methods=["PUT"]
)
@session_required
@content_type(["application/json"])
@csrf_check
def put_notifications(conversation_id):
    try:
        n = models.Notifications(**request.json)
        n.email = session["email"]
        cm = models.ConversationModel(conversation_id)
        nn = cm.set_notifications(session["uid"], n)
    except pydantic.ValidationError as exc:
        return problem_details_response(
            400, ProblemDetails.MALFORMED_REQUEST, str(exc)
        )
    except models.ConversationInactiveError as exc:
        return problem_details_response(
            403, ProblemDetails.CONVERSATION_INACTIVE, str(exc)
        )
    except models.NotificationsNotAvailableError as exc:
        return problem_details_response(
            403, ProblemDetails.NOTIFICATIONS_NOT_AVAILABLE, str(exc)
        )
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )
    except models.EmailAddressMissingError as exc:
        return problem_details_response(
            422, ProblemDetails.EMAIL_ADDRESS_MISSING, str(exc)
        )
    return jsonify(nn)


@bp.route(
    "/conversations/<conversation_id>/participant/notifications",
    methods=["GET"]
)
@auth_required
@cache_control(private=True, no_cache=True)
def get_notifications(conversation_id):
    if not have_session():
        # dummy value if authentication is disabled and no session has been
        # created yet
        return jsonify(models.Notifications())

    try:
        cm = models.ConversationModel(conversation_id)
        nn = cm.get_notifications(session["uid"])
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )
    return jsonify(nn)


@bp.route("/conversations/<conversation_id>/votes/<tid>", methods=["PUT"])
@session_required
@content_type(["application/json"])
@csrf_check
def votes(conversation_id, tid):
    try:
        v = models.Vote(**request.json)
        cm = models.ConversationModel(conversation_id)
        vv = cm.add_vote(session["uid"], tid, v)
    except pydantic.ValidationError as exc:
        return problem_details_response(
            400, ProblemDetails.MALFORMED_REQUEST, str(exc)
        )
    except models.ConversationInactiveError as exc:
        return problem_details_response(
            403, ProblemDetails.CONVERSATION_INACTIVE, str(exc)
        )
    except models.VotingNotAllowedError as exc:
        return problem_details_response(
            403, ProblemDetails.VOTING_NOT_ALLOWED, str(exc)
        )
    except models.ConversationNotFoundError as exc:
        return problem_details_response(
            404, ProblemDetails.NOT_FOUND, str(exc)
        )
    return jsonify(vv)
