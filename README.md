# Particiapi - Web API for alternative Polis frontends

Particiapi is a server which provides a web API enabling alternative,
third-party frontends for the [Polis project][1].

Features:

- web API using JSON
- cross-domain access via CORS
- OpenID Connect authentication

The server is implemented in Python 3 using the Flask framework and has the
following direct dependencies:

- Flask
- Flask-Cors
- psycopg
- Authlib
- requests
- pydantic

## Deployment

Install the dependencies by running `pip -c requirements.txt -r
requirements-base.txt`.  Create a directory `instance/`, copy
`config.py.example` to `instance/config.py` and change all mandatory settings.
Secrets can be generated securely with `python -c 'import secrets;
print(secrets.token_hex())'`.  The application can be served by any WSGI
compatible server, the entry point is `particiapi:create_app()`.

## License

Except specific files which bear a different mention, this project is licensed
under the EUPL-1.2 or later, see the included file `EUPL-1.2_EN.txt` for the
license text.


[1]: https://pol.is/home
